import pysony
import io
import Image
import sys
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np

def getImage(incoming):
	data = incoming.read(8)
	data = incoming.read(128)
	payload = pysony.payload_header(data)
	image_file = io.BytesIO(incoming.read(payload["jpeg_data_size"]))
	incoming.read(payload['padding_size'])
	return image_file

api = pysony.SonyAPI("http://192.168.122.1:10000/")
incoming = api.liveview()
	
fig = plt.figure()
img = Image.open("out.jpg")
rsize = img.resize((img.size[0], img.size[1]))
rsizeArr = np.asarray(rsize)
im = plt.imshow(rsizeArr, animated=True)

def update(*args):
	img = Image.open(getImage(incoming))
	rsize = img.resize((img.size[0], img.size[1]))
	rsizeArr = np.asarray(rsize)
	im.set_array(rsizeArr)
	return im,

ani = animation.FuncAnimation(fig, update, interval=25, blit=True)
plt.show()
